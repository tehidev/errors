package errors

import (
	"io"
	"runtime"
)

type withMessage struct {
	msg   string
	stack *runtime.Frames
	count int
}

func (e *withMessage) Error() string { return e.msg }

func (e *withMessage) Is(err error) bool {
	return e == err
}

func (e *withMessage) Unwrap() error {
	return nil
}

func (e *withMessage) Skip(n int) Error {
	for n > 0 {
		e.stack.Next()
		n--
		e.count--
	}
	return e
}

func (e *withMessage) Stack() *runtime.Frames { return e.stack }

func (e *withMessage) StackLines() []string {
	return getStackLines(e.stack, e.count)
}

func (e *withMessage) WriteTo(w io.Writer) (n int64, err error) {
	return writeStack(w, e.stack)
}

type withStack struct {
	error
	stack *runtime.Frames
	count int
}

func (e *withStack) Is(target error) bool {
	return e == target || e.error == target
}

func (e *withStack) Unwrap() error {
	return e.error
}

func (e *withStack) Skip(n int) Error {
	for n > 0 {
		e.stack.Next()
		n--
		e.count--
	}
	return e
}

func (e *withStack) Stack() *runtime.Frames { return e.stack }

func (e *withStack) StackLines() []string {
	return getStackLines(e.stack, e.count)
}

func (e *withStack) WriteTo(w io.Writer) (n int64, err error) {
	return writeStack(w, e.stack)
}
