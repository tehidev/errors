package errors

import (
	"fmt"
	"io"
	"runtime"
	"strconv"
)

func getStack() (stack *runtime.Frames, n int) {
	const depth = 32
	var pcs [depth]uintptr
	n = runtime.Callers(3, pcs[:])
	stack = runtime.CallersFrames(pcs[:n])
	return
}

func getStackLines(stack *runtime.Frames, n int) []string {
	lines := make([]string, 0, n)
	for {
		frame, more := stack.Next()
		lines = append(lines, frame.File+":"+strconv.Itoa(frame.Line))
		if !more {
			break
		}
	}
	return lines
}

func writeStack(w io.Writer, stack *runtime.Frames) (n int64, err error) {
	for {
		frame, more := stack.Next()
		dn, err := fmt.Fprintf(w, "%s:%d", frame.File, frame.Line)
		n += int64(dn)
		if err != nil {
			return n, err
		}
		if !more {
			break
		}
		_, _ = w.Write(endLine)
	}
	return 0, nil
}

var endLine = []byte{'\n'}
